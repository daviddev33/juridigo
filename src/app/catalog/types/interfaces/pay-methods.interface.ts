export enum Method {
  CASH ,
  CREDIT,
  PAYPAL
}
export interface PayMethod {
  label: string;
  value: Method;
}
