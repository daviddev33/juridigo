import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';

// Components
import {LawyerComponent} from './lawyer.component';


const routes: Routes = [
  {
    path: '',
    component: LawyerComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    RouterModule
  ],
  exports: [
    RouterModule
  ]
})
export class LawyerRoutingModule {
}
