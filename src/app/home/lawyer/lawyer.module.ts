import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { SwiperModule } from "swiper/angular";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Modules
import { LawyerRoutingModule } from './lawyer-routing.module';
import { MaterialDesignerModule } from '../../material-designer/material-designer.module';
import { UtilsModule } from 'app/utils/utils.module';
// Components
import { LawyerComponent } from './lawyer.component';

import { CommonComponentsModule } from '../common-components/common-components.module';
import { SharedModule } from 'app/shared/shared.module';



@NgModule({
  declarations: [
    LawyerComponent,
  
   
  
  
  ],
  imports: [
    CommonModule,
    LawyerRoutingModule,
    CommonComponentsModule,
    MaterialDesignerModule,
    UtilsModule,
    SharedModule,
    InlineSVGModule,
    SwiperModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    LawyerComponent,
   
  ]
})
export class LawyerModule { }
