import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';

// Components
import {ClientComponent} from './client.component';


const routes: Routes = [
  {
    path: '',
    component: ClientComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    RouterModule
  ],
  exports: [
    RouterModule
  ]
})
export class ClientRoutingModule {
}
