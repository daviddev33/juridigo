export interface Footer {
  title: string;
  icon: Icon[];
}

export interface Icon {
  url: string;
  url_active: string;
  name: string;
}

