interface Advantage {
    advantage: AdvantageElement[];
}

interface AdvantageElement {
    url?: string;
    title: string;
    text: string;
}

export {
  Advantage,
  AdvantageElement
};
