import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InlineSVGModule} from 'ng-inline-svg';
// Modules
import { ClientRoutingModule } from './client-routing.module';
import { UtilsModule } from 'app/utils/utils.module';
import { MaterialDesignerModule } from '../../material-designer/material-designer.module';
import { SwiperModule } from 'swiper/angular';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonComponentsModule } from '../common-components/common-components.module';
import { SharedModule } from 'app/shared/shared.module';

// Components
import { ClientComponent } from './client.component';





@NgModule({
  declarations: [
    ClientComponent
  
  ],
  imports: [
    CommonModule,
    MaterialDesignerModule,
    SwiperModule,
    ReactiveFormsModule,
    ClientRoutingModule,
    UtilsModule,
    FormsModule,
    InlineSVGModule,
    CommonComponentsModule,
    SharedModule
  ],
  exports: [
    ClientComponent
  ],
})
export class ClientModule {
}
