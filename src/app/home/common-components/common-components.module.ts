import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InlineSVGModule} from 'ng-inline-svg';

// Modules
import { SwiperModule } from 'swiper/angular';
import { MaterialDesignerModule } from 'app/material-designer/material-designer.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UtilsModule } from 'app/utils/utils.module';
// Components




import { LastNewsComponent } from './last-news/last-news.component';

import { ChatComponent } from './chat/chat.component';
import { MessageCatalogueComponent } from './message-catalogue/message-catalogue.component';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [
    


    LastNewsComponent,
   
    ChatComponent,
    
    MessageCatalogueComponent,
  ],
  imports: [
    CommonModule,
    SwiperModule,
    MaterialDesignerModule,
    ReactiveFormsModule,
    FormsModule,
    UtilsModule,
    InlineSVGModule,
    RouterModule
  ],
  exports: [
  
   
 
    LastNewsComponent,
  
    ChatComponent,
    
    MessageCatalogueComponent
  ]
})
export class CommonComponentsModule { }
