import {Component, Input, OnInit} from '@angular/core';
import {Comments} from 'app/home/client/types/interfaces';
import {Actions} from 'app/utils/types/interfaces';
import * as moment from 'moment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  @Input() comment: Comments;
  @Input() actionsComment: Actions;

  public flag: boolean = false;
  public flagAnswer: boolean = false;
  public thereAreAnswers: boolean;
  public reply: any;
  public dateComment: string;
  public dateAnswer: string;

  constructor() {
  }

  ngOnInit(): void {
    this.thereAreAnswers = this.getThereAreAnswers();
    this.dateComment = this.formatDate(this.comment?.createdAt);
    this.reply = this.getReplyToComment(this.comment);
    this.dateAnswer = this.formatDate(this.reply?.createdAt);
  }

  get numberAnswers() {
    if (!this.comment.answers)
      return;
    return this.comment.answers.length;
  }

  public getThereAreAnswers() {
    return this.numberAnswers > 0;
  }

  public openAndCloseAnswers() {
    this.flag = !this.flag;
  }

  public openForm() {
    this.flagAnswer = !this.flagAnswer;
  }

  public receiveCloseForm(value: boolean) {
    this.flagAnswer = value;
  }

  public formatDate(date: string) {
    return moment(date).format('DD MMM YYYY, h:mm A');
  }

  /**
   * receives a comment from the blog, maps the replies and returns each reply
   * @param comment
   */
  public getReplyToComment(comment: Comments) {
    return comment.answers?.map(reply => {
      return reply;
    });
  }

}
