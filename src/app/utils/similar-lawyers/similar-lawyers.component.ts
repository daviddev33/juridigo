import {UtilsService} from '../services/utils.service';
import {Component, OnInit} from '@angular/core';
import {LoadingService} from "../../shared/services/loading.service";
import {Lawyer, LawyerCardOptions} from "../../catalog/types/interfaces";

@Component({
  selector: 'app-similar-lawyers',
  templateUrl: './similar-lawyers.component.html',
  styleUrls: ['./similar-lawyers.component.scss']
})
export class SimilarLawyersComponent implements OnInit {
  public lawyers: Array<Lawyer>;
  public lawyerOptions: LawyerCardOptions = {
    background: "white-bg",
    contactOptions: false,
    description: false,
    premiumMark: false,
  };

  constructor(private utilsService: UtilsService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    await this.getData();
  }

  /**
   * get the list of the similar lawyers.
   * @private
   */
 private async getData() {
    this.loadingService.setLoading(true);
    try {

      const data = await this.utilsService.getSimilarLawyer();
      this.lawyers = data;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }
}
